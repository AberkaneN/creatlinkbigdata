package esipe.bigdata.com;

public class linkJoueur {
    private int j1;
    private int j2;
    private int playwith ;

    public linkJoueur(int j1, int j2, int playwith) {
        this.j1 = j1;
        this.j2 = j2;
        this.playwith = playwith;
    }

    public int getJ1() {
        return j1;
    }

    public void setJ1(int j1) {
        this.j1 = j1;
    }

    public int getJ2() {
        return j2;
    }

    public void setJ2(int j2) {
        this.j2 = j2;
    }

    public int getPlaywith() {
        return playwith;
    }

    public void setPlaywith(int playwith) {
        this.playwith = playwith;
    }

    @Override
    public String toString() {
        return "linkJoueur{" +
                "j1=" + j1 +
                ", j2=" + j2 +
                ", playwith=" + playwith +
                '}';
    }
}
