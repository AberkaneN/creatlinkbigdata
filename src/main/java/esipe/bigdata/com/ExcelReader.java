package esipe.bigdata.com;

/**
 * Hello world!
 *
 */

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ExcelReader {
        public static final String pathToCsv = "C:\\Users\\Aberkane\\BigData\\CreateLink\\src\\main\\java\\esipe\\bigdata\\com\\transfert-football.csv";

        public static void main(String[] args) throws IOException {
            String row;
            List<Joueur> joueurs= new ArrayList<>();
            BufferedReader csvReader = new BufferedReader(new FileReader(pathToCsv));
            int skip = 0;
            while ((row = csvReader.readLine()) != null) {
                if(skip ==1) {
                    String[] data = row.split(";");
                    int id = Integer.parseInt(data[0]);
                    Joueur j = new Joueur(id,data[1].toString(),data[2].toString(),data[3].toString(),data[4].toString(),null);
                    joueurs.add(j);
                }
                skip=1;
            }

            List<Joueur> joueurs2= joueurs;
            int i =0;
            for (Joueur unjoueur:joueurs) {
                List linkforthisplayer = new ArrayList();
                for(Joueur autrejoueur: joueurs2){
                    // si un jour a la même source ou destination durant la même année alors on cree un lien true 1  sion false 0
                    if(unjoueur.getNodeid() != autrejoueur.getNodeid() && unjoueur.getDate().equals(autrejoueur.getDate()) && (unjoueur.getDestination().equals(autrejoueur.getDestination()) || unjoueur.getSource().equals(autrejoueur.getSource()))){
                        i++;
                        linkforthisplayer.add(new linkJoueur(unjoueur.getNodeid(),autrejoueur.getNodeid(),1));

                    }else {
                        linkforthisplayer.add(new linkJoueur(unjoueur.getNodeid(),autrejoueur.getNodeid(),0));
                    }
                }
                unjoueur.setLink(linkforthisplayer);

            }

            for (Joueur unjoueur:joueurs) {

                    System.out.println(unjoueur.getNom() + "a jouer avec " + unjoueur.getAjouerAvec().toString() + " Période :" + unjoueur.getDate());

            }

        }

    }

