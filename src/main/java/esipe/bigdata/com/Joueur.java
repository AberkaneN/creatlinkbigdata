package esipe.bigdata.com;

import java.util.ArrayList;
import java.util.List;

public class Joueur {
    private int nodeid;
    private String nom;
    private String source;
    private String destination;
    private String date;
    private List<linkJoueur> link = new ArrayList<>();


    public Joueur(int nodeid, String nom, String source, String destination, String date, List<linkJoueur> link) {
        this.nodeid = nodeid;
        this.nom = nom;
        this.source = source;
        this.destination = destination;
        this.date = date;
        this.link = link;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public List<linkJoueur> getLink() {
        return link;
    }

    public void setLink(List<linkJoueur> link) {
        this.link = link;
    }

    public int getNodeid() {
        return nodeid;
    }

    public void setNodeid(int nodeid) {
        this.nodeid = nodeid;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<linkJoueur> getAjouerAvec(){
        List<linkJoueur> aJouerAvec = new ArrayList<>();
        int i=0;
        while (i<this.link.size()){
            if(link.get(i).getPlaywith()==1){
                aJouerAvec.add(link.get(i));
            }
            i++;
        }
        return aJouerAvec;
    }

    @Override
    public String toString() {
        return "Joueur{" +
                "nodeid=" + nodeid +
                ", nom='" + nom + '\'' +
                ", source='" + source + '\'' +
                ", destination='" + destination + '\'' +
                ", date='" + date + '\'' +
                ", link=" + link +
                '}';
    }
}
